package es.upm.emse.absd.agents;

import jade.core.Agent;

public class HelloAgent extends Agent {

    protected void setup()
    {
        System.out.println(hello(this.getLocalName()));
    }

    public String hello(String name) {
        return "Hello!!! My name is " + name + ". Nice to meet you :D";
    }

}
